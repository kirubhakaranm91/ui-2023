import React, { createContext, useContext, useState } from 'react'
import { useParams } from 'react-router-dom';

let initialValue = 0;
const person = {name:'Lion',age:2}
// const name = 'Tiger'
const arr = [1,2,3]
const bool = true;
const passValue = createContext("");
const NewCounter = ({children}:any) =>{
  return(
    <div>
      <h1>
        New Counter
        {children}
      </h1>
      <NewCounter1 />
    </div>
  )
}
const NewCounter1 = () =>{
  const name = useContext(passValue)
  return(
    <div>
      <h1>
        New Counter2 : 
        {name}
      </h1>
      <NewCounter2 />
    </div>
  )
}
const NewCounter2 = () =>{
  const name = useContext(passValue)
  // const { name } = props;
  return(
    <div>
      <h1>
        New Counter2 : 
        {name}
      </h1>
    </div>
  )
}
const Counter = () => {
    const [first, setFirst] = useState(person);
    const [second, setsecond] = useState(bool);
    const {id} = useParams() as any;
    // var first = 0;
    const increament = (e:any) =>{
      console.log(e);
      
        // setFirst(first + 1)
        // setFirst((a)=>a + 1)
        setFirst({...first,[e.target.name]:e.target.value})
        setsecond(!second)
    }
    // console.log(first); 
    // console.log("Rerender");
    
  return (
    <div>
        <h1>Counter</h1>
        <h1>name : {first.name}</h1>
        <h1>age : {first.age}</h1>
        <h1>{second}</h1>
        <label htmlFor="">Name :</label>
        <input name='name' type="text" onChange={increament}/><br /><br />
        <label htmlFor="">Age :</label>
        <input name='age' type="number" onChange={increament}/><br /><br />
        <button onClick={increament}>increament Original</button>
        <h2>Params id : {id}</h2>
        <passValue.Provider value={first.name}>
          <NewCounter>
            <h1>{first.name}</h1>
            <h2>{first.age}</h2>
          </NewCounter>
        </passValue.Provider>
    </div>
  )
}

export default Counter;