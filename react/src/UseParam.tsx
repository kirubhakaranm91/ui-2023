import React, { useEffect, useLayoutEffect, useState } from 'react'
import { useParams } from 'react-router-dom';

const UseParam = () => {
    const [posts, setPosts] = useState([]);
    const {ids} = useParams() as any;
    // const ids = 1

    useEffect(() => {
        fetch(`https://jsonplaceholder.typicode.com/posts/${ids}`)
      .then(response => response.json())
      .then(json => setPosts(json))
    }, [])
    console.log(posts);
    
  return (
    <div>
        {/* {posts.map((item:any)=>(
            <>
            <h3>{item.title}</h3>
            <p>{item.body}</p>
            </>
        ))} */}
        {Object.keys(posts).map((e:any)=>(
            <h1>{e === "body"? posts[e] : ''}</h1>
        ))}
    </div>
  )
}

export default UseParam