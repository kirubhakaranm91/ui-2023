import React, { useLayoutEffect, useState } from 'react'
import { Link, Route, Routes, useNavigate } from 'react-router-dom';
import Counter from './Counter';

const UseEffect = () => {
    const [mouseCoordinates, setMouseCoordinates] = useState({x:0, y:0});
    const [value, setValue] = useState([])
    const [newVal, setNewVal] = useState('')
	const navigate = useNavigate()
    const id = 1;
    const mouseMoveHandler = (event:any) => {
    //   setMouseCoordinates({
    //         x:event.clientX,
    //         y:event.clientY
    //   });
    }
    
    const movePage =()=>{
        navigate("/counter")
    }
    // useEffect(()=>{
    //     // fetch('https://jsonplaceholder.typicode.com/todos')
    //     // .then(response => response.json())
    //     // .then(json => setValue(json))
    //   window.addEventListener('mousemove', mouseMoveHandler);
    // //   return(()=>{
    // //         window.removeEventListener('mousemove', mouseMoveHandler);
    // //   })
    // console.log("hello");
    // },[])
    React.useEffect(() => {
        window.addEventListener('mousemove', mouseMoveHandler);
        console.log("Component did mount");
        let time = setInterval(()=>{
            let date = new Date(); 
            let hh = date.getHours();
            let mm = date.getMinutes();
            let ss = date.getSeconds();
            console.log(hh + ":" + mm+ ":" +ss);
        },1000)
        // fetch('https://jsonplaceholder.typicode.com/todos')
        //     .then(response => response.json())
        //     .then(json => setValue(json))
        console.log('inside of useeffect');
        
      return () => {
        window.removeEventListener('mousemove', mouseMoveHandler);
        clearInterval(time);
        console.log("component will unmount");
      }
    },[])
    
    React.useEffect(() => {
        console.log("second useeffect");
    }, [])

    useLayoutEffect(() => {
        console.log("5555");
    }, [])
    // console.log("outside of useeffect");
    
    return (
      <>
            Mouse Coordinates: x = {mouseCoordinates.x}, y={mouseCoordinates.y}<br />
            {/* <button onClick={movePage}>change component to counter</button> */}
            <Link to={`/counter/${id}`} >Change to counter</Link><br />
            {/* <a href="/counter">Change Comp</a> */}
            <input type="text" onChange={(e:any)=>setNewVal(e.target.value)}/>
            <tbody style={{width:'100%'}}>
            {value.map((e:any)=>(
                <tr key={e.id}>
                <td >{e.id}</td>
                <td >{e.title}</td>
                </tr>
            ))}
             </tbody>
             {/* <Routes>
                <Route path='/counter' element={<Counter />}/>
            </Routes> */}
      </>
    )
}

export default UseEffect;