// "use strict";
// document.getElementById('body').innerHTML = '<h1>Hello JS</h1>';
// alert('hi')
console.time()
console.log('Hello JS');
console.error('Hello');
console.info('JS');
console.warn('Hello JS')
console.table(['Hello','Js'])
console.log(console);
// let f, g, h = 5 + 2 + 6;
// console.log(f);
// console.log(g);
// console.log(h);
console.timeEnd()

// var a = 2 + 4 - 1 * 100 / 10 + (5 + 5) 
// console.log('a',a)

// 2 + 4 - 1 * 100 / 10 + ( 5 + 5 )\

//( 5 + 5) = 10
// 1 * 100 = 100
// 100 / 10 = 10
// 2 + 4 = 6
// 6 - 10 = -4
// - 4 + 10 = 6

// console.log(2**6)
// b = 2
// b += 5
// console.log('bbbbb',b)
// var a = 20;
function add(){
    var a;
    // fetch('https://jsonplaceholder.typicode.com/todos/1')
    //   .then(response => response.json())
    //   .then(json => a = json)
    // //   console.log('-----b',a)
    //   return a;
    // console.log(a)
//    let dom = document.getElementsByTagName('body')[0]
   document.getElementsByTagName('body')[0].innerHTML = '<h1>Hello JS</h1>'
}
console.log(add());
// console.log(add());
// console.log('++++',a);
// console.log('----',b);

// var a;
// var a = 10;
// let b = 20;
// let b = 30;

const COLOR_RED = "#F00";
const COLOR_GREEN = "#0F0";
const COLOR_BLUE = "#00F";
const COLOR_ORANGE = "#FF7F00";

color = COLOR_RED
color = COLOR_RED

function prompt (){
    let num1 = (prompt("enter first value : "))
    let num2 = (prompt('enter second value : '))

    let result = +num1 + +num2
    alert("The final result is " + result)
}

// alert( Boolean("") );

// let a = 'hh';
// alert(a??'--lol')

let a = 10;
let b = 20;
let c = 20;

// alert(a == 10 ? b == 20 ? c == 30 ? 'a' : 'b' : 'c' : 'd' );

let names = 'ajdksahdjaji'

let i = 0;

while(names[i] !== 's'){
    console.log(names[i]);
    i++;
}
console.log(add);
// add
add(8,9,7)

function add(){
    console.log(arguments);
    for (let index = 0; index < arguments.length; index++) {
        console.log(arguments[index]);
    }
}


function sub(a = 10,b = 5){
    // console.log('sub-->',a-b);
   return a + b;
    
}
console.log(sub('add---->',20,5))



var sayHi = function() {
    console.log( "Hello" );
  };

  console.log(sayHi());

// console.log(addition());
// console.log(subraction());

var addition;
let subraction;

var z = function () {
    return 5 + 5
}
// let z = 20;
let x = z();
console.log('---->x',x);

switch (z() !== 11) {
    case 10:
        console.log('switch---->10');
        break;
    case true:
        console.log('switch--->true');
        break;
    default:
        console.log('switch---->No10');
        break;
}

let arr = [1,2,3]
let d = [...arr];
d.push(4);
console.log('------->d',arr);
console.log('------->d',d);

function zoo(a,b){
    // debugger;
    let c = a();
    return c + b
}
function animal() {
    return 5 + 5
}
console.log('---->Zoo',zoo(animal,2));

const add1 = () => 5 + 12;
console.log('add1-------->',add1());

function call(){
    document.getElementById('error').innerText = ''
    let val = document.getElementById('inputs').value;
    console.log('val--------',val);
    if(val == ''){
    document.getElementById('error').innerText = 'Mandatory Field'}

}

// -------------  Javascript Objects and its methods -----------

let person = {
    name:'lion',
    age:20
}

let human = new Object();

console.log(person);
console.log(human);

Object.assign(human,person);

console.log(human);

let newPerson = Object.create(person)

console.log(newPerson.name)
console.log(newPerson)

console.log('hasOwn',Object.hasOwn(newPerson,'email'))

newPerson.email = 'lion@gmail.com'

console.log('hasOwn',Object.hasOwn(newPerson,'email'))

console.log(Object.getOwnPropertyDescriptor(person,'name'));

console.log(Object.defineProperty(person,'name',{enumerable:false}));

for (const key in person) {
  console.log(key);
}

console.log('getOwnPropertyDescriptors',Object.getOwnPropertyDescriptors(person));

console.log('defineProperties',Object.defineProperties(person,{name:{enumerable:true,writable:true},age:{enumerable:true}}));

for (const key in person) {
    console.log('enumerable iteration',key);
  }

console.log('person',person);

person.name = 'zebra'

console.log(person.name);

console.log(Object.defineProperty(person,'name',{writable:true}));

person.name = 'tiger'

console.log(person.name);

console.log('keys',Object.keys(person));

console.log('values',Object.values(person));

console.log('isFrozen',Object.isFrozen(person));

// Object.freeze(person)

console.log('isFrozen',Object.isFrozen(person));

person.name = 'lion'

console.log('freeze',person.name);

// console.log(Object.defineProperty(person,'name',{enumerable:false}));

console.log('getOwnPropertyDescriptors',Object.getOwnPropertyDescriptors(person));

let humans = {
    legs:2
}
humans.prototype = {name:'animal'}

console.log(humans,'humans');

console.log('setPrototypeOf',Object.setPrototypeOf(person,humans));

console.log('acess prototype',person.legs);

let newHuman = {
    name:'wick',
    __proto__:humans
}

console.log('newHuman',newHuman);

function person1(name,age) {
    this.name = name,
    this.age = age
}
person1.prototype = {companyName:'zoo company'}
let emp1 = new person1('lion',2)
let emp2 = new person1('tiger',3)

console.log(emp1.companyName);
console.log(emp2.companyName);

for (const key in humans) {
    if (Object.hasOwnProperty.call(humans,key)) {
       console.log('hasOwnProperty',key);
    }
    if(humans.hasOwnProperty(key)){
        console.log('hasOwnProperty',key);
    }
}

console.log(humans.prototype.isPrototypeOf(''));

console.log(Object.entries(person));

let user = {};

Object.assign(user,person,human,newHuman)

console.log(user.age,user.name);

let users = {
    name: "John",
    sizes: {
      height: 182,
      width: 50
    }
  };

  console.log(users);

  let newUser = Object.assign({},users)

  console.log('nested cloning',newUser);

  let clone = structuredClone(users);

  console.log(clone.sizes.width);

  function arithmetic(){
    let val = document.getElementById('add').innerText || document.getElementById('subtract').innerText
    console.log(val);
    switch (val) {
        case 'add':
             console.log(10+5); 
            break;
        case 'subtract':
            console.log(10-5);
            break;
        default:
            break;
    }
  }

let namess = 'abcdefghijk'

// let index = 0;
// while (namess[index] !== 'f') {
//     console.log(namess[index]);
//     index++;
// }

// for (let index = 0; namess.charAt[index] !== 'f'; index++) {
//         console.log(namess[index]);
// }

// for (var index = 0; index < 5; index++) {
//     setTimeout(() => {
//         console.log(index);
//     }, 0);
// }

// for (let index = 0; index < 5; index++) {
//     setTimeout(() => {
//         console.log(index);
//     }, 0);
// }

function argum(){
    console.log(arguments)
}

argum(1,2,3,4)

function* add(a,b,c){
    yield a;
    yield b;
    yield c;
}

let val = add(1,2,3)
console.log(val);
console.log(val.next());
console.log(val.next());
console.log(val.next());
console.log(val.next());