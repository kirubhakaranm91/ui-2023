import React, { useRef, useState } from 'react'
import { Route, Routes } from 'react-router-dom';
import Counter from './Counter';
import UseEffect from './UseEffect';

const UseRef = () => {
    const [name, setName] = useState();
    const names = useRef() as any;
    const nameFuc = () =>{
        console.log(names.current);
        
        setName(names.current?.value);
        names.current.value = '';
        names.current?.focus();
    }
  return (
    <div>
        <h1>{name}</h1><br />
        <input type="text" ref={names}/><br /><br />
        <button onClick={nameFuc}>Save Name</button>
     
      <UseEffect />
    </div>
  )
}

export default UseRef;