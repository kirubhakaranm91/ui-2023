import React, { useState } from 'react'
import { Link } from 'react-router-dom'

const Count = () => {
    const [count, setCount] = useState(1)
    const inc = () =>{
        setCount(count + 1)
    }
  return (
    <div>
        <h1>{count}</h1>
        <button onClick={inc}>Increament</button>
        <Link to={`/useParam/${count}`}>useParam</Link>
        <Link to={'/useEffect'}>UseEffect</Link>
    </div>
  )
}

export default Count