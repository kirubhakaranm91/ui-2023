import React, { useRef, useState } from 'react'

const SetTimeout = () => {
    const [counter, setCounter] = useState(0)
    // const [stop, setStop] = useState() as any;
    const timer = useRef() as any;
    const inc = () =>{
        timer.current = setInterval(() => {
            // setCounter(counter + 1)
            setCounter((counter)=>counter + 1)
        }, 1000);
        // setStop(count)
    }
    // if (counter === 5) {
    //     clearInterval(timer.current)
    // }
    const stop = () =>{
        clearInterval(timer.current)
    }
  return (
    <div>
        <h1>{counter}</h1>
        <button onClick={inc}>Increament</button>
        <button onClick={stop}>Stop</button>
    </div>
  )
}

export default SetTimeout