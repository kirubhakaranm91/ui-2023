import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Countersss from './Counter';
import UseRef from './UseRef';
import UseEffect from './UseEffect';
import SetTimeout from './setTimeout';
import { Route, Routes } from 'react-router-dom';
import Counter from './Counter';

function App() {
  return (
    <div className="App">
      <h1>Hello world!</h1>
      {/* <Countersss /> */}
      <UseRef />
      
      {/* <SetTimeout /> */}
    </div>
  );
}

export default App;
